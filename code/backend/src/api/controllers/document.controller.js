const httpStatus = require('http-status');
const { omit } = require('lodash');
const Document = require('../models/document.model');

exports.saveCreated = async (req, res) => {
  try {
    const doc = new Document(req.body);
    const savedDoc = await doc.save();
    res.status(httpStatus.CREATED);
    res.json(savedDoc);
  } catch (error) {
    console.log(error);
  }
};
exports.getDocument = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await Document.getById(documentId);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.list = async (req, res) => {
  try {
    const docs = await Document.list();
    res.json(docs);
  } catch (error) {
    console.log(error);
  }
};

