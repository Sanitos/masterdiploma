const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/document.controller');
const {
  saveCreated,
} = require('../../validations/document.validation');

const router = express.Router();

router
  .route('/')
  .post(controller.saveCreated);
router
  .route('/')
  .get(controller.list);
router
  .route('/:documentId')
  .get(controller.getDocument);


module.exports = router;
