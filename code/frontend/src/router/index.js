import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from "../views/Login/Index.vue"
import SignUp from "../views/Signup/Index"
import Profile from "../views/Profile/Index"
import {isAuthenticated} from "../utils";
import NewDocument from "../views/NewDocument/Index"
import Documents from "../views/Documents/Index"
import DocumentsDetailed from "../views/Documents/Detailed/Index"
import AdminPanel from "../views/AdminPanel/Index"
import DocumentSigning from "../views/DocumentSigning/Index"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/admin_panel/:type?',
    name: 'AdminPanel',
    component: AdminPanel,
    props: route => ({...route.params})

  },
  {
    path: '/new_document/:type',
    name: 'NewDocument',
    component: NewDocument,
    props: route => ({...route.params})
  },
  {
    path: '/documents',
    name: 'Documents',
    component: Documents
  },
  {
    path: '/documents/:id',
    name: 'DocumentsDetailed',
    component: DocumentsDetailed,
    props: route => ({...route.params})
  },
  {
    path: '/document_signing/:id',
    name: 'DocumentSigning',
    component: DocumentSigning,
    props: route => ({...route.params})
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  const allowedRoutes = ["Login", "SignUp"];
  if(allowedRoutes.includes(to.name) ||  isAuthenticated()){
    next();
  }else{
    next({name: "Login"})
  }
});

export default router
