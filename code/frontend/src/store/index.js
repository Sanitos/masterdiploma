/* ============
 * Vuex Store
 * ============
 *
 * The store of the application.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vue from "vue";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";
import auth from "./auth/index"
import user from "./user/index"
import document from "./document/index"
// Modules


Vue.use(Vuex);

// const debug = process.env.NODE_ENV !== "production";
const debug = true;

const store = new Vuex.Store({
  /**
   * Assign the modules to the store.
   */
  modules: {
    auth,
    user,
    document
  },

  /**
   * If strict mode should be enabled.
   */
  strict: debug,

  /**
   * Plugins used in the store.
   */
  plugins: debug ? [createLogger()] : [],
});

export default store;