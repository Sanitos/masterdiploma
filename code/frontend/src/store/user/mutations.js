import Vue from "vue";

export default {
    setCurrentUser: (state, payload) => {
        Vue.set(state, "currentUser", payload);
    },
};