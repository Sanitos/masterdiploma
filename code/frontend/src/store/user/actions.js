import {User} from "../resources";

export default {
    getUserInfo: ({ commit }, payload) => new Promise((resolve, reject)=>{
        User.getUserInfo().then(({body})=>{
            commit("setCurrentUser", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};