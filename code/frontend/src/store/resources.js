import Vue from "vue";
const backendUrl = (url) => {
    return `http://localhost:3000/${url}`
}

export const Auth = {
    login: (payload) => Vue.http.post(backendUrl("auth/login"), payload),
    signup: (payload) => Vue.http.post(backendUrl("auth/register"), payload)
};
export const User = {
    getUserInfo: () => Vue.http.get(backendUrl("users/profile"))
};
export const Document = {
    save: (payload) => Vue.http.post(backendUrl("documents"), payload),
    list: () => Vue.http.get(backendUrl("documents")),
    getDocument: (payload) => Vue.http.get(backendUrl("documents/{id}"), payload)
};