import {Document} from "../resources";

export default {
    save: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Document.save(payload).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    list: ({ commit }) => new Promise((resolve, reject)=>{
        Document.list().then(({body})=>{
            commit("setDocuments", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    getDocument: ({ commit }, {id}) => new Promise((resolve, reject)=>{
        Document.getDocument({params: {id}}).then(({body})=>{
            commit("setDocument", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};