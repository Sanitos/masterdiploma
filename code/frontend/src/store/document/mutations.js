import Vue from "vue";

export default {
    setDocuments: (state, payload) => {
        Vue.set(state, "list", payload);
    },
    setDocument: (state, payload) => {
        Vue.set(state, "document", payload);
    },
};