import {Auth} from "../resources";
import {setAuthToken} from "../../utils";

export default {
    login: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Auth.login(payload).then(({body})=>{
            const {tokenType, accessToken} = body.token;
            setAuthToken(`${tokenType} ${accessToken}`);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    signup: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Auth.signup(payload).then(({body})=>{
            const {tokenType, accessToken} = body.token;
            setAuthToken(`${tokenType} ${accessToken}`);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};